const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');

const resolveToPath = (toPath) => (...args) => path.join(toPath, ...args);

const resolveRoot = resolveToPath(process.cwd());
const resolveSrc = resolveToPath(path.join(process.cwd(), 'src'));
const resolveDist = resolveToPath(path.join(process.cwd(), 'dist'));
const resolveNodeModules = resolveToPath(path.join(process.cwd(), 'node_modules'));

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  resolve: {extensions: [".js", ".json"]},
  devServer: {
    contentBase: resolveDist()
  },
  entry: resolveSrc('index.js'),
  output: {
    path: resolveDist(),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "source-map-loader"
      },
      {
        test: /\.css$/,
        use: [{
          loader: 'style-loader'
        }, {
          loader: 'css-loader'
        }]
      }
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: resolveSrc('index.html')
    }),
    new CopyPlugin([
      {from: resolveSrc('version.json'),to: resolveDist('version.json')},
      {from: resolveSrc('config.template'),to: resolveDist('config.template')},
    ])
  ],
};
