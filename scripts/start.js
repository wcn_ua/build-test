const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const webpackConfig = require('../webpack.config.js');
const HOST = '0.0.0.0';
const PORT = '8081';

new WebpackDevServer(webpack(webpackConfig), {
  publicPath: '/',
  hot: true,
  historyApiFallback: true,
  disableHostCheck: true
}).listen(PORT, HOST, function (err, result) {
  if (err) {
    console.log(err);
  }
  console.log(`Listening at ${HOST}:${PORT}`);
});
