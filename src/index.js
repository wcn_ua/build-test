import './index.css';

console.log('### Hello there');

const myRequest = new Request('config.json');

fetch(myRequest).then(function(response) {
  const contentType = response.headers.get("content-type");
  if(contentType && contentType.includes("application/json")) {
    return response.json();
  }
  throw new TypeError("Oops, we haven't got JSON!");
})
  .then(function(json) {
    console.log(json.casino.name);
    const h = document.getElementById('hi');
    h.innerText = `Hi from ${json.casino.name}!`;
  })
  .catch(function(error) { console.log(error); });
